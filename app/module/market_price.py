from db import db
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from datetime import datetime
import re

engine = create_engine("sqlite:///db/db.sqlite3", echo=True)
Session = sessionmaker(bind=engine)
session = Session()

def help():
    f = open("/var/discord-bot/app/static/market_price/help.txt")
    help_text = f.read()
    f.close()
    return help_text

""" 登録アイテム一覧を返す
m[0] : command
m[1] : option (shop or item)
m[2] : value (str)
"""
def get_list(m):
    if len(m) == 1:
        result = session.query(db.MarketPrice).all()
    elif len(m) == 3 and m[1] == "shop":
        result = session.query(db.MarketPrice)\
                .filter(db.MarketPrice.shop.like("%"+m[2]+"%"))\
                .all()
    elif len(m) == 3 and m[1] == "item":
        result = session.query(db.MarketPrice)\
                .order_by(db.MarketPrice.price.asc())\
                .filter(db.MarketPrice.item.like("%"+m[2]+"%"))\
                .all()
    else:
        return "option over error"
    return result

""" アイテム追加
m[0] : command
m[1] : shop
m[2] : item
m[3] : price
m[4] : remarks
"""
def add_item(m):
    if len(m) == 4:
        query = db.MarketPrice(
            shop=m[1],
            item=m[2],
            price=m[3],
            date=datetime.now().strftime("%y%m%d"))
        session.add(query)
        session.commit()
        return "added!"
    elif len(m) == 5:
        query = db.MarketPrice(
            shop=m[1],
            item=m[2],
            price=m[3],
            date=datetime.now().strftime("%y%m%d"),
            remarks=m[4])
        session.add(query)
        session.commit()
        return "added!"
    else:
        return "option error"

""" アイテムを削除
m : id
"""
def delete_item(m):
    query = session.query(db.MarketPrice).get(m)
    if query:
        session.delete(query)
        session.commit()
        return "ID : "+m+" delete!"
    else:
        return "ID None!"

""" アイテム情報を修正
m[0] : command
m[1] : id
m[2] : shop
m[3] : item
m[4] : price
m[5] : remarks
"""
def update_item(m):
    query = session.query(db.MarketPrice).get(m[1])
    if query:
        if len(m) == 5:
            query.shop = m[2]
            query.item = m[3]
            query.price = int(m[4])
            query.date = datetime.now().strftime("%y%m%d")
        elif len(m) == 6:
            query.shop = m[2]
            query.item = m[3]
            query.price = int(m[4])
            query.remarks = m[5]
            query.date = datetime.now().strftime("%y%m%d")
        session.commit()
        return "ID : "+m[1]+" updated!"
    else:
        return "ID None!"

session.close()