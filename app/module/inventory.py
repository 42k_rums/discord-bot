from db import db
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
import re

engine = create_engine("sqlite:///db/db.sqlite3", echo=True)
Session = sessionmaker(bind=engine)
session = Session()

def help():
    f = open("/var/discord-bot/app/static/inventory/help.txt")
    help_text = f.read()
    f.close()
    return help_text

""" 在庫一覧を返す
m[0] : command
m[1] : option (int:stock[0-3] or str:place)
"""
def get_list(m):
    if len(m) == 1:
        result = session.query(db.Inventory).all()
    elif len(m) == 2:
        if re.match(r"[0-3]", m[1]):
            result = session.query(db.Inventory)\
                    .filter(db.Inventory.stock == m[1])\
                    .all()
        else:
            result = session.query(db.Inventory)\
                    .filter(db.Inventory.place == m[1])\
                    .all()
    else:
        return "option over error"
    return result

""" アイテムを追加
m[0] : command
m[1] : place
m[2] : item
m[3] : stock
"""
def add_item(m):
    if len(m) == 4 and re.match(r"[0-3]",m[3]):
        inv = db.Inventory(place=m[1],item=m[2],stock=m[3])
        session.add(inv)
        session.commit()
        return m[2]+" added!"
    else:
        return "option error"

""" アイテムを削除
m : id
"""
def delete_item(m):
    inv = session.query(db.Inventory).get(m)
    if inv:
        session.delete(inv)
        session.commit()
        return "ID : "+m+" deleted!"
    else:
        return "ID None!"

""" アイテム情報を修正
m[0] : command
m[1] : id
m[2] : place
m[3] : item
m[4] : stock
"""
def update_item(m):
    inv = session.query(db.Inventory).get(m[1])
    if inv:
        inv.place = m[2]
        inv.item = m[3]
        inv.stock = int(m[4])
        session.commit()
        return "ID : "+m[1]+" updated!"
    else:
        return "ID None!"

""" 指定したIDのストック数のみを更新
m[0] : command
m[1] : id
m[2] : stock
"""
def update_stock_item(m):
    inv = session.query(db.Inventory).get(m[1])
    if inv:
        inv.stock = int(m[2])
        session.commit()
        return "ID : "+m[1]+" stock updated!"
    else:
        return "ID None!"
        
""" 指定したIDのストックを3(余裕)に更新
m[0] : command
m[1] : id
"""
def fill_up(m):
    inv = session.query(db.Inventory).get(m[1])
    if inv:
        inv.stock = 3
        session.commit()
        return "ID : "+m[1]+" fill up!"
    else:
        return "ID None!"

session.close()
