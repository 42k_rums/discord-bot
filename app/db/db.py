from sqlalchemy import create_engine, MetaData, Table, Column, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

Base = declarative_base()

class Inventory(Base):
    __tablename__ = "inventory"

    id = Column(Integer, primary_key=True)
    place = Column(String, nullable=False)
    item = Column(String, nullable=False, unique=True)
    stock = Column(Integer, nullable=False)

    def __repr__(self):
        return "<Inventory(id='%s', place='%s, item='%s', stock='%s')>" % (self.id, self.place, self.item, self.stock)

class MarketPrice(Base):
    __tablename__ = "market_price"

    id = Column(Integer, primary_key=True)
    shop = Column(String, nullable=False)
    item = Column(String, nullable=False)
    price = Column(Integer, nullable=False)
    date = Column(String, nullable=False)
    remarks = Column(String)

    def __repr__(self):
        return "<MarketPrice(id='%s', shop='%s', item='%s', price='%s', date='%s', remarks='%s')>" % (self.id, self.shop, self.item, self.price, self.date, self.remarks)

if __name__ == "__main__":
    engine = create_engine("sqlite:///db.sqlite3", echo=True)
    Base.metadata.create_all(engine)