import discord, asyncio
import configparser
from module import inventory, market_price

config = configparser.ConfigParser()
config.read("./instance/config")
discord_token = config.get("discord","token")

client = discord.Client()

@client.event
async def on_ready():
    print(client.user.id, client.user.name, "login!")
    print("----------")

@client.event
async def on_message(message):

    print("message.content", message.content)
    print("message.channel", message.channel)

    sending_channel0_id = config.get("discord","channel0_id")
    sending_channel1_id = config.get("discord","channel1_id")
    sending_msg = ""

    ### 在庫管理チャンネル ###
    if str(message.channel.id) == sending_channel0_id:
        m = message.content.split()
        print(m)
        command = m[0]
        sending_msg = ""

        if command in ["help", "つかいかた"]:
            sending_msg = inventory.help()
        elif command in ["list", "一覧"]:
            inv_list = inventory.get_list(m)
            if not inv_list == "empty database":
                for inv in inv_list:
                    line = "["+str(inv.id)+"] "+inv.place+" "+inv.item+" "+str(inv.stock)+"\n"
                    sending_msg += line
            else:
                sending_msg = inv_list
        elif command in ["add", "追加"]:
            sending_msg = inventory.add_item(m)
        elif command in ["delete", "削除"]:
            sending_msg = inventory.delete_item(m[1])
        elif command in ["update", "更新"]:
            sending_msg = inventory.update_item(m)
        elif command in ["fill", "補充"]:
            sending_msg = inventory.fill_up(m)
        elif command in ["stock", "ストック"]:
            sending_msg = inventory.update_stock_item(m)
        else:
            sending_msg = "Unknown command, Type in \"help\" or \"つかいかた\""
        
        if client.user != message.author:
            if not sending_msg:
                sending_msg = "Empty message..."
            await client.send_message(message.channel, sending_msg)

    ### 相場管理チャンネル ###
    if str(message.channel.id) == sending_channel1_id:
        m = message.content.split()
        command = m[0]
        sending_msg = ""

        if command in ["help", "つかいかた"]:
            sending_msg = market_price.help()
        elif command in ["list", "一覧"]:
            price_list = market_price.get_list(m)
            if not price_list == "empty database":
                for i in price_list:
                    line = "["+str(i.id)+"] "+i.shop+" "+i.item+" "+str(i.price)+"円 "+i.date+" "+str(i.remarks)+"\n"
                    sending_msg += line
            else:
                sending_msg = price_list
        elif command in ["add", "追加"]:
            sending_msg = market_price.add_item(m)
        elif command in ["delete", "削除"]:
            sending_msg = market_price.delete_item(m[1])
        elif command in ["update", "更新"]:
            sending_msg = market_price.update_item(m)
        else:
            sending_msg = "Unknown Command, Type in help or つかいかた"

        if client.user != message.author:
            if not sending_msg:
                sending_msg = "Empty message..."
            await client.send_message(message.channel, sending_msg)

client.run(discord_token)