FROM python:3.6.5
MAINTAINER 42k
RUN apt-get update
RUN apt-get install sqlite3
RUN pip install -U discord.py
RUN pip install gunicorn
RUN pip install sqlalchemy
RUN mkdir /var/discord-bot
WORKDIR /var/discord-bot